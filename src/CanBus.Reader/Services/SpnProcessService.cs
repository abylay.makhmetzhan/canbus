using System.Collections.Generic;
using System.Linq;
using CanBus.Reader.Data;
using CanBus.Reader.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Riwo.Rimote.VirtualCan.J1939;

namespace CanBus.Reader.Services
{
    public class SpnProcessService
    {
        private const int ByteSize = 8;

        private readonly List<Pgn> _pgns;
        private readonly ILogger<SpnProcessService> _logger;
        private readonly IBackgroundTaskQueue _pgnQueue;

        public SpnProcessService(ApplicationDbContext context, ILogger<SpnProcessService> logger, IBackgroundTaskQueue pgnQueue)
        {
            _logger = logger;
            _pgnQueue = pgnQueue;

            _pgns = context.Pgns
                .Include(s => s.PgnSpns)
                .ThenInclude(s => s.Spn)
                .ToList();
        }

        public void Process(J1939Frame frame)
        {
            var pgn = _pgns.SingleOrDefault(s => s.PgnCode == frame.Pgn);
            if (pgn == null)
            {
                _logger.LogWarning($"Unknown PGN {frame.Pgn}");
                return;
            }

            if (pgn.Size != frame.DataLength)
            {
                _logger.LogWarning($"Length PGN not equals {frame.Pgn}, db = {pgn.Size}, frame = {frame.DataLength}");
                return;
            }

            var pgnDto = new PgnDto()
            {
                Name = pgn.Name
            };

            foreach (var pgnSpn in pgn.PgnSpns)
            {
                var spn = GetSpn(frame, pgnSpn);
                pgnDto.Spns.Add(spn);

                _logger.LogInformation($"PGN = {pgn.PgnCode}\tSPN = {pgnSpn.Spn.SpnCode}\t value = {spn.Value}\tname = {spn.Name}");
            }

            _pgnQueue.QueueBackgroundWorkItem(pgnDto);
        }

        private SpnDto GetSpn(J1939Frame frame, PgnSpn pgnSpn)
        {
            int position = (int)((pgnSpn.Position - 1) * 0.125);
            byte bitPosition = (byte)(pgnSpn.Position - 1 - (position * ByteSize));
            long value = 0;

            for (int i = 0; i < pgnSpn.Spn.Size; i += ByteSize, position++)
            {
                byte count = pgnSpn.Spn.Size - i >= ByteSize
                    ? (byte) ByteSize
                    : (byte) (pgnSpn.Spn.Size - i);
                var bits = ReadBits(frame.Data[position], bitPosition, count);
                value = ((long) bits << i) | value;
            }

            var result = pgnSpn.Spn.Offset + pgnSpn.Spn.Resolution * value;

            return new SpnDto
            {
                Name = pgnSpn.Spn.Name + pgnSpn.Description,
                Value = result
            };
        }

        private byte ReadBits(byte val, byte p, byte n)
        {
            byte mask = (byte)((1 << (n + p)) - 1);
            return (byte)((val & mask) >> p);
        }
    }
}
