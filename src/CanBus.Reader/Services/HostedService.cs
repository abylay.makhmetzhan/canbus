using System;
using System.Buffers;
using System.IO.Pipelines;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Riwo.Rimote.VirtualCan;
using Riwo.Rimote.VirtualCan.J1939;
using Riwo.Rimote.VirtualCan.Linux;

namespace CanBus.Reader.Services
{
    public class HostedService: BackgroundService
    {
        private readonly string _adapter;
        private readonly ILogger<HostedService> _logger;
        private readonly J1939FrameProcessService _j1939FrameProcessService;

        public HostedService(string adapter, ILogger<HostedService> logger, J1939FrameProcessService j1939FrameProcessService)
        {
            _adapter = adapter;
            _j1939FrameProcessService = j1939FrameProcessService;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            var factory = new SocketCanFactory();
            using var socket = factory.CreateSocket(_adapter);
            await ProcessCanFramesAsync(socket, cancellationToken);
        }

        private Task ProcessCanFramesAsync(Socket socket, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Hosted Service is starting.");

            var pipe = new Pipe();
            var writing = FillPipeAsync(socket, pipe.Writer, cancellationToken);
            var reading = ReadPipeAsync(pipe.Reader, cancellationToken);

            return Task.WhenAll(reading, writing);
        }

        private async Task FillPipeAsync(Socket socket, PipeWriter writer, CancellationToken cancellationToken)
        {
            while (true)
            {
                var memory = writer.GetMemory(CanFrame.FrameLength);
                try
                {
                    var bytesRead = await socket.ReceiveAsync(memory, SocketFlags.None, cancellationToken);

                    // Tell the PipeWriter how much was read from the Socket
                    writer.Advance(bytesRead);
                }
                catch (OperationCanceledException)
                {
                    _logger.LogInformation("Pipe writer closing...");
                    break;
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    break;
                }

                // Make the data available to the PipeReader
                var result = await writer.FlushAsync(cancellationToken);

                if (result.IsCompleted) break;
                if (cancellationToken.IsCancellationRequested) break;
            }

            // Tell the PipeReader that there's no more data coming
            writer.Complete();
        }

        private async Task ReadPipeAsync(PipeReader reader, CancellationToken cancellationToken)
        {
            while (true)
            {
                try
                {
                    var result = await reader.ReadAsync(cancellationToken);
                    var buffer = result.Buffer;

                    var incomingFrame = new J1939Frame(buffer.Slice(0, CanFrame.FrameLength).ToArray());
                    ProcessFrame(incomingFrame);

                    buffer = buffer.Slice(CanFrame.FrameLength);

                    // Tell the PipeReader how much of the buffer we have consumed
                    reader.AdvanceTo(buffer.Start);

                    // Stop reading if there's no more data coming
                    if (result.IsCompleted) break;
                    if (cancellationToken.IsCancellationRequested) break;
                }
                catch (OperationCanceledException)
                {
                    _logger.LogInformation("Pipe reader closing...");
                    break;
                }
                catch (Exception e)
                {
                    LogError(e);
                    break;
                }
            }

            // Mark the PipeReader as complete
            reader.Complete();
        }

        private void LogError(Exception exception)
        {
            _logger.LogError(exception, "Pipe error");
        }

        private void ProcessFrame(J1939Frame incomingFrame)
        {
            _j1939FrameProcessService.Process(incomingFrame);
        }
    }
}
