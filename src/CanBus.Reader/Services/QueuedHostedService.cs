using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CanBus.Reader.Models;
using CanDataGrpc;
using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CanBus.Reader.Services
{
    public class QueuedHostedService : BackgroundService
    {
        public const string GrpcHostKey = "GrpcHost";

        private readonly string _grpcHost;
        private readonly ILogger _logger;
        private readonly IBackgroundTaskQueue _pgnQueue;

        public QueuedHostedService(ILogger<QueuedHostedService> logger, IConfiguration configuration, IBackgroundTaskQueue pgnQueue)
        {
            _grpcHost = configuration.GetValue<string>(GrpcHostKey);
            _logger = logger;
            _pgnQueue = pgnQueue;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(_grpcHost))
            {
                _logger.LogInformation("Queued Hosted Service disabled.");
                return;
            }

            _logger.LogInformation("Queued Hosted Service is starting.");

            var channel = GrpcChannel.ForAddress(_grpcHost);
            var client =  new CanData.CanDataClient(channel);

            while (!cancellationToken.IsCancellationRequested)
            {
                var pgnDto = await _pgnQueue.DequeueAsync(cancellationToken);
                var data = GetPgnData(pgnDto);

                try
                {
                    await client.SendDataAsync(data);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Error occurred executing {pgnDto.Name}.");
                    _pgnQueue.QueueBackgroundWorkItem(pgnDto);
                }
            }

            _logger.LogInformation("Queued Hosted Service is stopping.");
        }

        private static ParameterGroupNumber GetPgnData(PgnDto pgnDto)
        {
            var data = new ParameterGroupNumber
            {
                Name = pgnDto.Name
            };
            data.Spns.AddRange(pgnDto.Spns.Select(s => new SuspectParameterNumber
            {
                Name = s.Name,
                Value = s.Value
            }));
            return data;
        }
    }
}
