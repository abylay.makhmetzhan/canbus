using System.Threading;
using System.Threading.Tasks;
using CanBus.Reader.Models;

namespace CanBus.Reader.Services
{
    public interface IBackgroundTaskQueue
    {
        void QueueBackgroundWorkItem(PgnDto workItem);

        Task<PgnDto> DequeueAsync(CancellationToken cancellationToken);
    }
}
