using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using CanBus.Reader.Models;
using Microsoft.Extensions.Configuration;

namespace CanBus.Reader.Services
{
    public class BackgroundTaskQueue : IBackgroundTaskQueue
    {
        private readonly ConcurrentQueue<PgnDto> _workItems = new ConcurrentQueue<PgnDto>();
        private readonly SemaphoreSlim _signal = new SemaphoreSlim(0);
        private readonly bool _ignoreAdd;

        public BackgroundTaskQueue(IConfiguration configuration)
        {
            _ignoreAdd = string.IsNullOrWhiteSpace(configuration.GetValue<string>(QueuedHostedService.GrpcHostKey));
        }

        public void QueueBackgroundWorkItem(PgnDto workItem)
        {
            if (_ignoreAdd) return;

            if (workItem == null)
            {
                throw new ArgumentNullException(nameof(workItem));
            }

            _workItems.Enqueue(workItem);
            _signal.Release();
        }

        public async Task<PgnDto> DequeueAsync(CancellationToken cancellationToken)
        {
            await _signal.WaitAsync(cancellationToken);
            _workItems.TryDequeue(out var workItem);

            return workItem;
        }
    }
}
