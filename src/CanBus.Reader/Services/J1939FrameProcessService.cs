using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Riwo.Rimote.VirtualCan.J1939;

namespace CanBus.Reader.Services
{
    public class J1939FrameProcessService
    {
        private readonly Dictionary<uint, J1939BamFrame> _frames = new Dictionary<uint, J1939BamFrame>();

        private readonly ILogger<J1939FrameProcessService> _logger;
        private readonly SpnProcessService _spnProcessService;

        public J1939FrameProcessService(ILogger<J1939FrameProcessService> logger, SpnProcessService spnProcessService)
        {
            _logger = logger;
            _spnProcessService = spnProcessService;
        }

        public void Process(J1939Frame frame)
        {
            J1939BamFrame bamFrame;

            if (_frames.ContainsKey(frame.Pgn))
            {
                bamFrame = _frames[frame.Pgn];
                try
                {
                    bamFrame.AddCanFrame(frame);
                }
                catch (Exception e)
                {
                    _logger.LogWarning(e, "Incorrect append frame");
                }

                if (bamFrame.IsFull)
                {
                    _frames.Remove(frame.Pgn);
                    ProcessFrame(bamFrame);
                }

                return;
            }

            if (J1939BamFrame.IsBamFrame(frame))
            {
                try
                {
                    _frames.Add(frame.Pgn, J1939BamFrame.Create(frame));
                }
                catch (Exception e)
                {
                    _logger.LogWarning(e, "Incorrect create frame");
                }
                return;
            }

            ProcessFrame(frame);
        }

        private void ProcessFrame(J1939Frame frame)
        {
            PrintFrame(frame);
            _spnProcessService.Process(frame);
        }

        private void PrintFrame(J1939Frame frame)
        {
            _logger.LogInformation($"PGN = {Convert.ToString(frame.Pgn, 16)}\tIsBAM = {frame is J1939BamFrame}");
            _logger.LogInformation(ByteArrayToString(frame.Data));
        }

        private static string ByteArrayToString(ReadOnlySpan<byte> ba)
        {
            var hex = new StringBuilder(ba.Length * 3);

            foreach (var t in ba)
            {
                hex.AppendFormat($" {t:X2}");
            }

            return hex.ToString();
        }
    }
}
