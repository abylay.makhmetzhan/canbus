namespace CanBus.Reader.Models
{
    public class SpnDto
    {
        public string Name { get; set; }

        public double Value { get; set; }
    }
}
