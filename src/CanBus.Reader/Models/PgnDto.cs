using System.Collections.Generic;

namespace CanBus.Reader.Models
{
    public class PgnDto
    {
        public string Name { get; set; }

        public List<SpnDto> Spns { get; } = new List<SpnDto>();
    }
}
