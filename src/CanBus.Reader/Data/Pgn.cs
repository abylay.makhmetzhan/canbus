using System.Collections.Generic;

namespace CanBus.Reader.Data
{
    public class Pgn
    {
        public int Id { get; set; }

        public int PgnCode { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Длина указывается в байтах
        /// </summary>
        public int Size { get; set; }

        public virtual ICollection<PgnSpn> PgnSpns { get; set; }
    }
}
