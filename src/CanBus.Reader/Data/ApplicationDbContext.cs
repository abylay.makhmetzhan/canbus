using Microsoft.EntityFrameworkCore;

namespace CanBus.Reader.Data
{
    public sealed class ApplicationDbContext: DbContext
    {
        public DbSet<Pgn> Pgns { get; set; }

        public DbSet<Spn> Spns { get; set; }

        public DbSet<PgnSpn> PgnSpns { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            :base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Pgn>()
                .Property(s => s.Name)
                .HasMaxLength(255)
                .IsRequired();

            modelBuilder.Entity<Spn>()
                .Property(s => s.Name)
                .HasMaxLength(255)
                .IsRequired();

            modelBuilder.Entity<PgnSpn>()
                .Property(s => s.Description)
                .HasMaxLength(255);
        }
    }
}
