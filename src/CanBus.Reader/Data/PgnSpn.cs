namespace CanBus.Reader.Data
{
    public class PgnSpn
    {
        public int Id { get; set; }

        /// <summary>
        /// В позиция в битах
        /// </summary>
        public int Position { get; set; }

        public string Description { get; set; }

        public int PgnId { get; set; }

        public virtual Pgn Pgn { get; set; }

        public int SpnId { get; set; }

        public virtual Spn Spn { get; set; }
    }
}
