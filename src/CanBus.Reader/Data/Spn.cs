using System.Collections.Generic;

namespace CanBus.Reader.Data
{
    public class Spn
    {
        public int Id { get; set; }

        public int SpnCode { get; set; }

        /// <summary>
        /// Длина указывается в битах
        /// </summary>
        public int Size { get; set; }

        public double Resolution { get; set; }

        public int Offset { get; set; }

        public string Name { get; set; }

        public virtual ICollection<PgnSpn> PgnSpns { get; set; }
    }
}
