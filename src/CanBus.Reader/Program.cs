using System;
using System.IO;
using System.Threading.Tasks;
using CanBus.Reader.Data;
using CanBus.Reader.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace CanBus.Reader
{
    internal static class Program
    {
        public static async Task Main(string[] args)
        {
            try
            {
                await CreateHostBuilder(args).RunConsoleAsync();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            new HostBuilder()
                .ConfigureHostConfiguration(configHost =>
                {
                    configHost.SetBasePath(Directory.GetCurrentDirectory());
                    configHost.AddJsonFile("hostsettings.json", optional: true);
                    configHost.AddEnvironmentVariables(prefix: "DOTNETCORE_");
                    configHost.AddCommandLine(args);
                })
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddEnvironmentVariables();
                    config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                    config.AddJsonFile($"appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", optional: true, reloadOnChange: true);
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    Log.Logger = new LoggerConfiguration()
                        .Enrich.FromLogContext()
                        .ReadFrom.Configuration(hostingContext.Configuration)
                        .CreateLogger();

                    logging.AddSerilog();
                })
                .ConfigureServices((hostContext, services) =>
                {
                    RegisterServices(args, services, hostContext.Configuration);
                })
                .UseConsoleLifetime();

        private static void RegisterServices(string[] args, IServiceCollection services, IConfiguration configuration)
        {
            var adapter = args.Length > 0 ? args[0] : "vcan0";

            services.AddHostedService<QueuedHostedService>();
            services.AddHostedService(s => new HostedService(adapter,
                s.GetRequiredService<ILogger<HostedService>>(),
                s.GetRequiredService<J1939FrameProcessService>()));
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlite(configuration.GetConnectionString("DefaultConnection")));

            services.AddSingleton<J1939FrameProcessService>();
            services.AddSingleton<SpnProcessService>();
            services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();
        }
    }
}
