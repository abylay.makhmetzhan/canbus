"use strict";

var data = {
  flowmeter: [0, 0, 0],
  temperature: [0]
};
var connection = new signalR.HubConnectionBuilder().withUrl("/canDataHub").build();

connection.on("ReceiveData", function (pgn) {
  updateChartData(pgn);
  appendText(pgn);
});

connection.start()
  .catch(function (err) {
    return console.error(err.toString());
  });

function appendText(pgn) {
  var text = pgn.name + '\n';
  pgn.spns.forEach(function (spn) {
    text += "\t- " + spn.name + ": \t" + spn.value + "\n";
  });

  var textarea = document.getElementById('logs');
  textarea.value = textarea.value.slice(-10000) + text + "\n";
}

function updateChartData(pgn){
  if(pgn.name === "Расходомер топлива. Счетчики 2"){
    data.flowmeter[1] = pgn.spns[1].value;
  } else if(pgn.name === "Отфильтрованные уровень и объем топлива в баке"){
    data.flowmeter[0] = pgn.spns[1].value;
    data.flowmeter[2] = pgn.spns[2].value;
    data.temperature[0] = pgn.spns[3].value;
  }
}

function getFlowmeterData(){
  return data.flowmeter;
}

function getTemperature(){
  return data.temperature;
}
