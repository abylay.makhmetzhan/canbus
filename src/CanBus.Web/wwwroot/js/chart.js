var flowmeterCtx = document.getElementById('flowmeterChart').getContext('2d');
var flowmeterChart = new Chart(flowmeterCtx, {
  type: 'line',

  data: {
    labels: [],
    datasets: [{
      label: 'Объём в баке',
      borderColor: 'rgb(13,176,45)',
      backgroundColor: 'rgb(13,176,45)',
      fill: false,
      data: []
    }, {
      label: 'Суммарный расход',
      borderColor: 'rgb(255, 99, 132)',
      backgroundColor: 'rgb(255, 99, 132)',
      fill: false,
      data: []
    }, {
      label: 'Часовой расход',
      borderColor: 'rgb(165,51,255)',
      backgroundColor: 'rgb(165,51,255)',
      fill: false,
      data: []
    }]
  },

  // Configuration options go here
  options: {
    title: {
      display: true,
      text: 'Топливо'
    }
  }
});

var temperatureCtx = document.getElementById('temperatureChart').getContext('2d');
var temperatureChart = new Chart(temperatureCtx, {
  type: 'line',

  data: {
    labels: [],
    datasets: [{
      label: 'Температура топлива',
      borderColor: 'rgb(226,24,12)',
      backgroundColor: 'rgb(226,24,12)',
      fill: false,
      data: []
    }]
  },

  // Configuration options go here
  options: {
    title: {
      display: true,
      text: 'Температура'
    }
  }
});

function addData(chart, label, data) {
  chart.data.labels.push(label);
  for (var i = 0; i < chart.data.datasets.length; i++){
    chart.data.datasets[i].data.push(data[i]);
  }
}

function removeData(chart) {
  chart.data.labels.shift();
  chart.data.datasets.forEach(function(dataset) {
    dataset.data.shift();
  });
}

function updateChart(chart, label, data) {
  addData(chart, label, data);
  if(chart.data.labels.length > 30) removeData(chart);
  chart.update();
}

function update() {
  var label = getLabel();

  updateChart(flowmeterChart, label, getFlowmeterData());
  updateChart(temperatureChart, label, getTemperature());
}

function getLabel(){
  var d = new Date();
  return d.getHours().toString().padStart(2, '0')
    + ':' + d.getMinutes().toString().padStart(2, '0')
    + ':' + d.getSeconds().toString().padStart(2, '0');
}

setInterval(update, 2000);
