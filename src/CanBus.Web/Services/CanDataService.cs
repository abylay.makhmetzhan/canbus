using System.Threading.Tasks;
using CanBus.Web.Hubs;
using CanDataGrpc;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace CanBus.Web.Services
{
    public class CanDataService: CanData.CanDataBase
    {
        private readonly ILogger<CanDataService> _logger;
        private readonly IHubContext<CanDataHub> _hubContext;

        public CanDataService(ILogger<CanDataService> logger, IHubContext<CanDataHub> hubContext)
        {
            _logger = logger;
            _hubContext = hubContext;
        }

        public override async Task<Empty> SendData(ParameterGroupNumber pgn, ServerCallContext context)
        {
            _logger.LogInformation($"Receive pgn name = {pgn.Name}");
             await _hubContext.Clients.All.SendAsync("ReceiveData", pgn);
            return new Empty();
        }
    }
}
