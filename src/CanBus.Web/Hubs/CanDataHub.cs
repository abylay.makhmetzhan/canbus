using System.Threading.Tasks;
using CanDataGrpc;
using Microsoft.AspNetCore.SignalR;

namespace CanBus.Web.Hubs
{
    public class CanDataHub: Hub
    {
        public async Task SendMessage(ParameterGroupNumber pgn)
        {
            await Clients.All.SendAsync("ReceiveData", pgn);
        }
    }
}
