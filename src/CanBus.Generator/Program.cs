﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using McMaster.Extensions.CommandLineUtils;
using Riwo.Rimote.VirtualCan.Linux;

namespace CanBus.Generator
{
    internal static class Program
    {
        private static async Task<int> Main(string[] args)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            var app = new CommandLineApplication();

            app.HelpOption();
            var optionCanAdapter = app
                .Option("-a|--adapter <adapter>", "Can adapter (default: vcan0)", CommandOptionType.SingleValue);
            var optionTimeout = app
                .Option("-t|--timeout <ms>", "Timeout pgn frame (default: 250)", CommandOptionType.SingleValue);

            app.OnExecuteAsync(async token =>
            {
                var timeout = TimeSpan.FromMilliseconds(optionTimeout.HasValue() ? int.Parse(optionTimeout.Value()) : 250);
                var adapter = optionCanAdapter.HasValue() ? optionCanAdapter.Value() : "vcan0";
                await GenerateCanDataAsync(adapter, timeout, token);
                return 0;
            });

            return await app.ExecuteAsync(args, cancellationTokenSource.Token);
        }

        private static async Task GenerateCanDataAsync(string adapter, TimeSpan timeout, CancellationToken cancellationToken)
        {
            var frameGenerator = new CanFrameGenerator();
            var factory = new SocketCanFactory();

            using var socket = factory.CreateSocket(adapter);
            {
                Console.WriteLine($"Created adapter: {adapter}");

                while (!cancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        await Task.Delay(timeout, cancellationToken);
                        var sendFrame = frameGenerator.GetFrames();

                        foreach (var frame in sendFrame)
                        {
                            await socket.SendAsync(frame.FrameBytes, SocketFlags.None);
                            await Task.Delay(TimeSpan.FromMilliseconds(10), cancellationToken);

                            Console.WriteLine($"  {adapter}  {Convert.ToString(frame.Id, 16).ToUpper()}   [{frame.DataLength}] {ByteArrayToString(frame.Data)}");
                        }
                    }
                    catch (SocketException se) when (se.SocketErrorCode == SocketError.NoBufferSpaceAvailable)
                    {
                        Console.WriteLine("No buffer space available waiting a few ms");
                    }
                }

                Console.WriteLine("Closing");
            }
        }

        private static string ByteArrayToString(ReadOnlySpan<byte> ba)
        {
            var hex = new StringBuilder(ba.Length * 3);

            foreach (var t in ba)
            {
                hex.AppendFormat($" {t:X2}");
            }

            return hex.ToString();
        }
    }
}
