using System;
using Riwo.Rimote.VirtualCan.J1939;

namespace CanBus.Generator
{
    public class CanFrameGenerator
    {
        private readonly Random _random = new Random();
        private int _current = 0;

        public J1939Frame[] GetFrames()
        {
            switch (_current)
            {
                case 0:
                    _current++;
                    return new[] { GetPgn62982() };
                case 1:
                    _current++;
                    return new[] { GetPgn62983() };
                case 2:
                    _current++;
                    return GetPgn62993().GetFrames();
                case 3:
                    _current = 0;
                    return GetPgn62998().GetFrames();
            }

            throw new Exception();
        }

        private J1939Frame GetPgn62982()
        {
            var frame = new J1939Frame { Id = 0x0CF606FE, IsExtendedFrame = true, DataLength = 8 };

            {
                const int max = 64255;
                var value = _random.Next(max);
                frame.Data[0] = (byte)(value & 0xFFFF);
                frame.Data[1] = (byte)((value >> 8) & 0xFFFF);
            }

            {
                const int max = 64255;
                var value = _random.Next(max);
                frame.Data[2] = (byte)(value & 0xFFFF);
                frame.Data[3] = (byte)((value >> 8) & 0xFFFF);
            }

            {
                const int max = 64255;
                var value = _random.Next(max);
                frame.Data[4] = (byte)(value & 0xFFFF);
                frame.Data[5] = (byte)((value >> 8) & 0xFFFF);
            }

            {
                const int max = 250;
                var value = _random.Next(max);
                frame.Data[6] = (byte)(value & 0xFF);
            }

            frame.Data[7] = 0xFF;
            return frame;
        }

        private J1939Frame GetPgn62983()
        {
            var frame = new J1939Frame { Id = 0x0CF607FE, IsExtendedFrame = true, DataLength = 8 };

            {
                const int max = 64255;
                var value = _random.Next(max);
                frame.Data[0] = (byte)(value & 0xFFFF);
                frame.Data[1] = (byte)((value >> 8) & 0xFFFF);
            }

            {
                const int max = 15;
                var value = _random.Next(max);
                frame.Data[2] = (byte)(value & 0xF);
            }

            for (int i = 3; i < frame.DataLength; i++)
            {
                frame.Data[i] = 0xFF;
            }

            return frame;
        }

        private J1939BamFrame GetPgn62993()
        {
            // 0CF611FE# 20 51 00 0C FF 11 F6 00
            var f = new J1939Frame { Id = 0x0CF611FE, IsExtendedFrame = true, DataLength = 8 };
            f.Data[0] = 0x20;
            f.Data[1] = 0x51;
            f.Data[2] = 0x00;
            f.Data[3] = 0x0C;
            f.Data[4] = 0xFF;
            f.Data[5] = 0x11;
            f.Data[6] = 0xF6;
            f.Data[7] = 0x00;

            var frame = J1939BamFrame.Create(f, 12);
            frame.Id = 0x0CF611FE;

            {
                const int max = 250;
                var value = _random.Next(max);
                frame.Data[0] = (byte)(value & 0xFF);
            }

            for (int i = 1; i < 81; i+= 4)
            {
                const int max = 4211080;
                var value = (uint)(_random.Next(max) / 0.001);
                frame.Data[i] = (byte)(value & 0xFFFF);
                frame.Data[i+1] = (byte)((value >> 8) & 0xFFFF);
                frame.Data[i+2] = (byte)((value >> 16) & 0xFFFF);
                frame.Data[i+3] = (byte)((value >> 24) & 0xFFFF);
            }

            return frame;
        }

        private J1939BamFrame GetPgn62998()
        {
            // 0CF616FE# 20 18 00 04 FF 16 F6 00
            var f = new J1939Frame { Id = 0x0CF616FE, IsExtendedFrame = true, DataLength = 8 };
            f.Data[0] = 0x20;
            f.Data[1] = 0x18;
            f.Data[2] = 0x00;
            f.Data[3] = 0x04;
            f.Data[4] = 0xFF;
            f.Data[5] = 0x16;
            f.Data[6] = 0xF6;
            f.Data[7] = 0x00;

            var frame = J1939BamFrame.Create(f, 4);
            frame.Id = 0x0CF616FE;

            {
                const int max = 4211080;
                var value = (uint)(_random.Next(max) / 0.001);
                frame.Data[0] = (byte)(value & 0xFFFF);
                frame.Data[1] = (byte)((value >> 8) & 0xFFFF);
                frame.Data[2] = (byte)((value >> 16) & 0xFFFF);
                frame.Data[3] = (byte)((value >> 24) & 0xFFFF);
            }

            {
                const int max = 4211010; //4211010000
                var value = (uint)(_random.Next(max) / 0.001);
                frame.Data[4] = (byte)(value & 0xFFFF);
                frame.Data[5] = (byte)((value >> 8) & 0xFFFF);
                frame.Data[6] = (byte)((value >> 16) & 0xFFFF);
                frame.Data[7] = (byte)((value >> 24) & 0xFFFF);
            }

            {
                const int max = 4211010; //4211010000
                var value = (uint)(_random.Next(max) / 0.001);
                frame.Data[8] = (byte)(value & 0xFFFF);
                frame.Data[9] = (byte)((value >> 8) & 0xFFFF);
                frame.Data[10] = (byte)((value >> 16) & 0xFFFF);
                frame.Data[11] = (byte)((value >> 24) & 0xFFFF);
            }

            for (int i = 12; i < 22; i+= 2)
            {
                const int max = 64254;
                var value = _random.Next(max);
                frame.Data[i] = (byte)(value & 0xFFFF);
                frame.Data[i+1] = (byte)((value >> 8) & 0xFFFF);
            }

            {
                const int max = 15;
                var value = _random.Next(max);
                frame.Data[22] = (byte)(value & 0xFFFF);
            }

            {
                const int max = 250;
                var value = _random.Next(max);
                frame.Data[23] = (byte)(value & 0xFFFF);
            }

            return frame;
        }
    }
}
