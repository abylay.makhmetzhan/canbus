namespace Riwo.Rimote.VirtualCan.J1939
{
    public class J1939Frame: CanFrame
    {
        public uint Priority
        {
            get => Id >> 26 & 0x7;
            set => Id = SetBits(Id, value, 26, 0x7);
        }

        public uint DataPage
        {
            get => Id >> 24 & 0x1;
            set => Id = SetBits(Id, value, 24, 0x1);
        }

        public uint PduFormat
        {
            get => Id >> 16 & 0xFF;
            set => Id = SetBits(Id, value, 16, 0xFF);
        }

        public uint PduSpecific
        {
            get => Id >> 8 & 0xFF;
            set => Id = SetBits(Id, value, 8, 0xFF);
        }

        public uint SourceAddress
        {
            get => Id & 0xFF;
            set => Id = SetBits(Id, value, 0, 0xFF);
        }

        public PduType PduType => PduFormat < 240 ? PduType.PDU1 : PduType.PDU2;

        public uint Pgn
        {
            get => PduType == PduType.PDU1
                ? (DataPage << 9) + (PduFormat << 8)
                : (DataPage << 9) + (PduFormat << 8) + PduSpecific;
            set
            {
                DataPage = (value >> 9) & 0x1;
                PduFormat = (value >> 8) & 0xFF;
                PduSpecific = value & 0xFF;
            }
        }

        public J1939Frame(byte[] frame) : base(frame)
        {
        }

        public J1939Frame() : this(new byte[FrameLength])
        {
        }
    }
}
