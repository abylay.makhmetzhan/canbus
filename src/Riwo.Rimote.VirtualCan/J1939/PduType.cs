namespace Riwo.Rimote.VirtualCan.J1939
{
    public enum PduType
    {
        None,
        PDU1,
        PDU2
    }
}
