using System;

namespace Riwo.Rimote.VirtualCan.J1939
{
    public class J1939BamFrame: J1939Frame
    {
        private const int Bam = 32;
        private const int BamFrameDataLength = 7;
        private const int BamFrameStart = FrameStart + 1;

        private int _frameCount = 0;

        public byte FrameCount => FrameBytes[FrameStart + 3];

        public bool IsFull => FrameCount == _frameCount;

        public override Span<byte> Data => new Span<byte>(FrameBytes, FrameLength, DataLength);

        public override int DataLength
        {
            get => FrameBytes[FrameStart+1] | (FrameBytes[FrameStart+2] << 8);
            set
            {
                var dataLength = FrameCount * BamFrameDataLength;
                if(dataLength < value  || (FrameBytes.Length - FrameLength) < value)
                    throw new ArgumentException($"Data length too long, change {nameof(FrameCount)}");

                FrameBytes[4] = 8;
                FrameBytes[FrameStart+1] = (byte)(value & 0xFF);
                FrameBytes[FrameStart+2] = (byte)(value >> 8 & 0xFF);
            }
        }

        private J1939BamFrame(byte[] frame) : base(frame)
        {
        }

        public void AddCanFrame(J1939Frame frame)
        {
            if (frame.Data[0] == 0 || frame.Data[0] > FrameCount)  throw new ArgumentException($"Incorrect append {nameof(frame)}");

            _frameCount = frame.Data[0];
            int insertPosition = (_frameCount - 1) * BamFrameDataLength + FrameLength;
            Array.Copy(frame.FrameBytes, BamFrameStart, FrameBytes, insertPosition, BamFrameDataLength);
        }

        public J1939Frame[] GetFrames()
        {
            var frames = new J1939Frame[FrameCount + 1];
            frames[0] = new J1939Frame(FrameBytes.AsSpan(0, FrameLength).ToArray());

            for (byte i = 1; i < frames.Length; i++)
            {
                var bytes = new byte[FrameLength];
                int startPosition = (i - 1) * BamFrameDataLength + FrameLength;
                bytes[FrameStart] = i;
                Array.Copy(FrameBytes, 0, bytes, 0, FrameStart);
                Array.Copy(FrameBytes, startPosition, bytes, BamFrameStart, BamFrameDataLength);

                frames[i] = new J1939Frame(bytes);
            }

            return frames;
        }

        public static J1939BamFrame Create(J1939Frame frame, byte frameCount = 0)
        {
            frameCount = frameCount == 0 ? frame.Data[3] : frameCount;
            var dataLength = frameCount * BamFrameDataLength;
            var size = dataLength + FrameLength;

            if (size < (BamFrameDataLength + FrameLength)) throw new ArgumentException($"Incorrect create {nameof(frame)}");

            var bytes = new byte[size];
            frame.FrameBytes.CopyTo(bytes, 0);

            return new J1939BamFrame(bytes);
        }

        public static bool IsBamFrame(J1939Frame frame)
        {
            var isControlByteBam = frame.Data[0] == Bam;
            var isFullFrame = frame.DataLength == FrameDataLength;

            if (!isControlByteBam || !isFullFrame) return false;

            var pgn = (frame.Data[7] << 16) | (frame.Data[6] << 8) | frame.Data[5];
            return frame.Pgn == pgn;
        }
    }
}
